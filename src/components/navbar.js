import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../App.css';
import axios from 'axios'

class Navbar extends Component {
    logout = (event) => {
        event.preventDefault()
        axios.post('/api/logout')
            .then(response => {
            if (response.status === 200) {
                this.props.updateUser({
                    loggedIn: false,
                    username: null
                })
            }
        }).catch(error => {
            console.log('Logout error')
        })
    }

    render() {
        const loggedIn = this.props.loggedIn

        return (
            <div>

                <header className="navbar App-header" id="nav-container">
                    <div className="col-4">
                        {loggedIn ? (
                            <section className="navbar-section">
                                <Link to="#" className="btn btn-link text-secondary" onClick={this.logout}>
                                    <span className="text-secondary">logout</span></Link>

                            </section>
                        ) : (
                            <section className="navbar-section">
                                {/* if you read this line of code stand up now and do the monkey moves :P */}
                                <Link to="/" className="btn btn-link text-secondary">
                                    <span className="text-secondary">Home</span>
                                </Link>
                                <Link to="/login" className="btn btn-link text-secondary">
                                    <span className="text-secondary">logIn</span>
                                </Link>
                                <Link to="/signup" className="btn btn-link">
                                    <span className="text-secondary">signUp</span>
                                </Link>
                            </section>
                        )}
                    </div>
                    <div className="col-4 col-mr-auto">
                        <div id="top-filler"></div>
                        <h1 className="App-title">Task App</h1>
                    </div>
                </header>
            </div>

        );

    }
}

export default Navbar