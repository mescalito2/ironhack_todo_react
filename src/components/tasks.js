import React, { Component } from 'react'
import axios from 'axios'

const Task = props => (
    <div>
        <li>
            <span>Title: </span>
            <span>{props.task.title} - </span>
            <span>Description: </span>
            <span>{props.task.description}. </span>
            {props.loggedIn &&
                <div>
                    <button onClick={props.toUpdate}>Edit</button>
                    <span> - </span>
                    <button onClick={props.toDelete}>Remove</button>
                </div>
            }
        </li>
    </div>
);

class Tasks extends Component {
    constructor() {
        super()
        this.state = {
            tasks: []
        }

        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.fetchTasks()
    }

    XHRequest = async (uri, method, postData) => {
        let response = {}
        try {
            if (method === 'get') {
                response = await axios.get(uri)
            } else {
                response = await axios.post(uri, postData)
            }
        } catch (error) {
            console.error(error)
            return {}
        }
        if (response.data.message) {
            alert(response.data.message)
            return response.data
        }
        return response
    }

    fetchTasks = async () => {
        this.setState({
            tasks: []
        });
        const response = await this.XHRequest("/api/tasks", 'get');
        this.setState({
            tasks: response.data
        });
    }

    deleteTask = async (id) => {
        const response = await this.XHRequest("api/tasks/delete/" + id, 'post', {id});
        if (response.status === 200) {
            this.setState({
                tasks: this.state.tasks.filter(task => task._id !== response.data._id)
            });
        }
    }

    updateTask = async (id) => {
        this.setState({
            tasks: this.state.tasks.map(task => {
                if (task._id !== id) return task;

                const title = prompt("type new TITLE", task.title);
                const description = prompt("type new DESCRIPTION", task.description);

                const updateDate = {
                    _id: task._id,
                    title: title,
                    description: description
                }

                this.XHRequest("api/tasks/edit/" + id, 'post', updateDate);
                return updateDate
            })
        });
    }

    addTask = async () => {
        const title = prompt("type new TITLE");
        const description = prompt("type new DESCRIPTION");
        if (description !== null && title !== null) {
            const newTasksData = {
                title: title,
                description: description
            }
            const response = await this.XHRequest("api/tasks/create", 'post', newTasksData);
            if (response.status === 200) {
                this.setState({
                    tasks: [
                        ...this.state.tasks,
                        {
                            _id: response.data._id,
                            title: response.data.title,
                            description: response.data.description
                        }
                    ]
                })
            }
        }
    }

    render() {

        const loggedIn = this.props.loggedIn
        return (
            <div>
                <div>Task count: {this.state.tasks.length}</div>
                {loggedIn &&
                    <button onClick={() => this.addTask()}>Add task</button>
                }
                <ul>
                    {this.state.tasks.map((task, i) => (
                        <Task
                            key={i}
                            toUpdate={() => this.updateTask(task._id)}
                            toDelete={() => this.deleteTask(task._id)}
                            task={task}
                            loggedIn={loggedIn}
                        />
                    ))}
                </ul>
            </div>
        )
    }
}

export default Tasks
