import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import axios from 'axios'
import { Route } from 'react-router-dom'
// components
import Signup from './components/sign-up'
import Tasks from './components/tasks'
import LoginForm from './components/login-form'
import Navbar from './components/navbar'

//style
import 'spectre.css/dist/spectre.min.css';
import 'spectre.css/dist/spectre-icons.css';
import './index.css';

class App extends Component {
    constructor() {
        super()
        this.state = {
            loggedIn: false,
            username: null,
            tasks: []
        }
    }

    componentDidMount = () => {
        this.getUser()
    }

    updateUser = (userObject) => {
        this.setState(userObject)
    }

    XHRequest = async (uri, method, postData) => {
        let response = {}
        try {
            if (method === 'get') {
                response = await axios.get(uri)
            } else {
                response = await axios.post(uri, postData)
            }
        } catch (error) {
            console.error(error)
            return {}
        }
        if (response.data.message) {
            alert(response.data.message)
            return response.data
        }
        return response
    }

    getUser = async () => {
        const response = await this.XHRequest("/api/loggedin", 'get');
        if (typeof response.data !== "undefined") {
            if (response.data.username) {
                this.setState({
                    loggedIn: true,
                    username: response.data.username
                })
            } else {
                this.setState({
                    loggedIn: false,
                    username: null
                })
            }
        }
    }

    render() {
        return (
            <div className="App">
                <Navbar updateUser={this.updateUser} loggedIn={this.state.loggedIn}/>
                {/* logged in? greet user */}
                {this.state.loggedIn &&
                <p>Welcome, {this.state.username}!</p>
                }
                {/* Routing different components */}
                <Route
                    path="/"
                    render={() =>
                        <Tasks
                            loggedIn={this.state.loggedIn}
                        />
                    }
                />
                <Route
                    path="/login"
                    render={() =>
                        <LoginForm
                          updateUser={this.updateUser}
                        />
                    }
                />
                <Route
                    path="/signup"
                    render={() =>
                        <Signup
                          updateUser={this.updateUser}
                        />
                    }
                />
            </div>
        );
    }
}

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('root')
)